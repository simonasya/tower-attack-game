#include "score.h"

Score::Score(int attackers, int towers) 
: destroyedTowers(0), killedAttackers(0), attackers(attackers), towers(towers) {}

Score::~Score(){}

int Score::Points() const{
	return destroyedTowers * TOWER_DESTROYED_POINTS;
}

int Score::getDestroyedTowers() const{
	return destroyedTowers;
}

int Score::getKilledAttackers() const{
	return killedAttackers;
}

int Score::getAttackers() const{
	return attackers;
}

int Score::getTowers() const{
	return towers;
}

void Score::savedAttacker(){
	attackers--;
}

Score & Score::operator ++ (int x){
	destroyedTowers++;
	towers--;
	return *this;
}

Score & Score::operator -- (int x){
	killedAttackers++;
	attackers--;
	return *this;
}

void Score::setCurrentAttacker(Attacker * attacker){
	this -> currentAttacker = attacker;
}

int Score::getAmmo()const{
	if(!currentAttacker) return 0;
	return currentAttacker -> getAmmo();
}
int Score::getLives()const{
	if(!currentAttacker)
		return 0;
	return currentAttacker -> getLives();
}