#ifndef SCORE_VIEW
#define SCORE_VIEW

#include <unistd.h>
#include "score.h"
#include "../functions.h"

const int SCORE_BOX_HEIGHT = 8, SCORE_BOX_WIDTH = 11;
const int SCORE_BOX_X = 1, SCORE_BOX_Y = 4;

/**
 * \brief represents user interface of score 
 */

class ScoreView{
public:
/**
 * \brief initialises instance of score view
 */
	ScoreView();
/**
 * \brief frees memory and destroy subwindow of score
 */
	~ScoreView();
/**
 * \brief shows score in subwindow
 * \param score source for data
 */
	void show(Score * score);
/**
 * \brief updates values in score view
 * \details called from show()
 * \param score source for data
 */
	void update(const Score * score);
/**
 * \brief deletes and closes subwindow of score view
 */
	void deleteScoreBox();
private:
	WINDOW * scoreBox; /**<score subwindow*/
};

#endif