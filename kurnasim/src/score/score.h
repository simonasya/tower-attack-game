#ifndef SCORE
#define SCORE

#include "../entity/attacker.h"

const int TOWER_DESTROYED_POINTS = 10; /**<points for destroying one of the towers*/

/**
 * \brief represents score of one level
 * \details provides information of current attacker and state of level
 */

class Score{
public:
/**
 * \brief initialises score instance with number of available attackers and towers
 * \param attackers number of attackers
 * \param towers number of towers to shoot
 */
	Score(int attackers, int towers);
/**
 * \brief frees memory of score
 */
	~Score();
/**
 * \brief counts points achieved in the current level
 * \return number of points
 */
	int Points() const;
/**
 * \return number of destroyed towers
 */
	int getDestroyedTowers() const;
/**
 * \return number of killed attackers
 */
	int getKilledAttackers() const;
/**
 * \return number of still living attackers
 */
	int getAttackers() const;
/**
 * \return number of still active towers
 */
	int getTowers() const;
/**
 * \brief is called when attacker left map through exit, without being shot
 * \details decreases count of active attackers, but does not increases number of killed ones
 */
	void savedAttacker();
/**
 * \brief called when tower destroyed
 * \details increases number of destroyed towers and decreases number of active towers
 */
	Score & operator ++ (int x);
/**
 * \brief called when attacker was killed
 * \details increases number of killed attackers and decreases number of active attackers
 */
	Score & operator -- (int x);
/**
 * \brief adds information of currently active attacker for ammunition and lives information
 * \param attacker pointer to currently active attacker
 */
	void setCurrentAttacker(Attacker * attacker);
/**
 * \return ammunition left of currently active attacker
 */
	int getAmmo()const;
/**
 * \return lives left of currently active attacker
 */
	int getLives()const;
private:
	int destroyedTowers; /**<count of destroyed towers*/
	int killedAttackers; /**<count of killed attackers*/
	int attackers; /**<count of active attackers*/
	int towers; /**<count of active towers*/
	Attacker * currentAttacker; /**<active attacker, currently in map*/
};

#endif