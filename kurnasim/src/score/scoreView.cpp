#include "scoreView.h"

ScoreView::ScoreView() : scoreBox(nullptr) {}

ScoreView::~ScoreView(){
	deleteScoreBox();
}

void ScoreView::show(Score * score){
	if(!scoreBox)
		scoreBox = newwin(SCORE_BOX_HEIGHT + 2,  SCORE_BOX_WIDTH + 2, SCORE_BOX_Y, SCORE_BOX_X);
	box(scoreBox, '|', '-');
	update(score);
}

void ScoreView::update(const Score * score){
	mvwprintw(scoreBox, 1, 1, "Killed: %d ", score -> getDestroyedTowers());
	mvwprintw(scoreBox, 2, 1, "Dead: %d ", score -> getKilledAttackers());
	mvwprintw(scoreBox, 3, 1, "Points: %d ", score -> Points());
	mvwprintw(scoreBox, 4, 1, "Attack: %d ", score -> getAttackers());
	mvwprintw(scoreBox, 5, 1, "Towers: %d ", score -> getTowers());
	mvwprintw(scoreBox, 6, 1, "-----------");
	mvwprintw(scoreBox, 7, 1, "AMMO: %d ", score -> getAmmo());
	mvwprintw(scoreBox, 8, 1, "LIVES: %d ", score -> getLives());
	
	wrefresh(scoreBox);
}

void ScoreView::deleteScoreBox(){
	if(scoreBox)
		deleteBox(scoreBox);
}