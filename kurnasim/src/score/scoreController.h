#ifndef SCORE_CONTROLLER
#define SCORE_CONTROLLER

#include "score.h"
#include "scoreView.h"

/**
 * \brief keeps track of current score and hands over to view if needed
 */

class ScoreController{
public:
/**
 * \brief initialise class and sets values of attackers and tower numbers
 * \param attackers number of available attackers for the level
 * \param towers number of towers for the level
 */
	ScoreController(int attackers, int towers);
/**
 * \brief frees memory
 */	
	~ScoreController();
/**
 * \brief shows score status in a window - calls scoreView instance
 */
	void show();
/**
 * \brief deletes score status from window
 */
	void deleteScoreBox();
/**
 * \brief is called when attacker is safely in exit and is still alive
 */
	void savedAttacker();
/**
 * \brief checks game status
 * \return true if player won
 */
	bool win() const;
/**
 * \brief checks game status
 * \return true if player lost
 */
	bool loss() const;
/**
 * \brief takes currently active attacker for status information source
 * \details does not free memory of this attacker
 * \param attacker current attacker
 */
	void setCurrentAttacker(Attacker * attacker);
/**
 * \brief is called when tower was destroyed, adds points and updates game status
 */
	ScoreController & operator ++ (int x);
/**
 * \brief is called when attacker was killed, updates game status
 */
	ScoreController & operator -- (int x);
/**
 * \brief retrieve points achieved in current level
 * \return number of points
 */
	int Points() const;
private:
	Score * score;
	ScoreView * scoreView;
};

#endif