#include "scoreController.h"

ScoreController::ScoreController(int attackers, int towers) 
: score(new Score(attackers, towers)), scoreView(new ScoreView()) {}

ScoreController::~ScoreController(){
	delete score;
	delete scoreView;
}

void ScoreController::show(){
	scoreView -> show(score);
}

void ScoreController::deleteScoreBox(){
	scoreView -> deleteScoreBox();
}

void ScoreController::savedAttacker(){
	score -> savedAttacker();
}

bool ScoreController::win() const{
	return score -> getTowers() == 0;
}

bool ScoreController::loss() const{
	if(score -> getTowers() > 0){
		if(score -> getAttackers() <= 0)
			return true;
	}
	return false;
}

void ScoreController::setCurrentAttacker(Attacker * currentAttacker){
	score -> setCurrentAttacker(currentAttacker);
}

ScoreController & ScoreController::operator ++ (int x){
	(*score) ++;
	return *this;
}

ScoreController & ScoreController::operator -- (int x){
	(*score) --;
	return *this;
}

int ScoreController::Points() const{
	return score -> Points();
}