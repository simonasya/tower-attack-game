#include "tower.h"

Tower::Tower(int color, int ammo, int lives, int x, int y) : Shooter(color, ammo, lives, x, y) {}

char Tower::getRepresentation() const{
	return TOWER_REPRESENTATION;
}

int Tower::behaviour() const{
	return ENEMY;
}

bool Tower::shoot() {
	return true;
}