#include "shooter.h"

Shooter::Shooter(int color, int ammo, int lives, int x, int y) 
: Entity(color), ammo(ammo), lives(lives), x(x), y(y) {}

bool Shooter::shoot(){
	if(ammo > 0){
		ammo--;
		return true;
	}
	return false;
}

bool Shooter::shot(){
	lives --;
	return true;
}

bool Shooter::isDead(){
	if(lives <= 1) setColor(COLOR_RED);
	return lives <= 0;
}

int Shooter::getAmmo() const{
	return ammo;
}

int Shooter::getLives() const{
	return lives;
}