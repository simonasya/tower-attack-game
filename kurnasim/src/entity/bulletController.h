#ifndef BULLET_CONTROLLER
#define BULLET_CONTROLLER

#include <list>

#include "../map/map.h"
#include "shootPosition.h"
#include "../entity/entity.h"
#include "../score/scoreController.h"

using namespace std;

/**
 * \brief responsible for managing bullets
 * \details stores bullet information and moves them toward shoot direction
 */

class BulletController{
public:
/**
 * \brief creates empty instance of bulletController
 */
	BulletController();
/**
 * \brief frees allocated memory
 */
	~BulletController();
/**
 * \brief updades position of all currently active bullets
 * \param map instance of current map
 * \param score instance of level score
 */
	void updatePosition(Map * map, ScoreController * score);
/**
 * \brief adds bullet to list
 * \param x X starting coordinate
 * \param y Y starting coordinate
 * \param dirX X direction of bullet {-1,0,1} 
 * \param dirY Y direction of bullet {-1,0,1} 
 * \param shootTowers true if capable of shooting towers
 */
	void addBullet(int x, int y, int dirX, int dirY, bool shootTowers);
/**
 * \brief removes not active bullets from list
 */
	void removeDeactive();
private:
	list<ShootPosition*> activeBullet; /**<list of active bullets on map*/
};

#endif