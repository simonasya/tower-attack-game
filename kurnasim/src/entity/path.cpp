#include "path.h"

Path::Path(int color) : Entity(color) {}

char Path::getRepresentation() const{
	if(visitor) 
		return visitor -> getRepresentation();
	return PATH_REPRESENTATION;
}

int Path::behaviour() const{
	if(visitor) 
		return visitor -> behaviour();
	return PATH;
}

bool Path::shoot(){
	if(visitor) 
		return visitor -> shoot();
	return false;
}

bool Path::shot(){
	if(visitor) 
		return visitor -> shot();
	return false;
}

bool Path::isDead(){
	if(visitor) return visitor -> isDead();
	return false;
}