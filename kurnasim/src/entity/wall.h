#ifndef WALL
#define WALL

#include "entity.h"

/**
 * \brief represents wall entity
 */

class Wall : public Entity{
public:
/**
 * \brief initialises wall entity
 * \param color color of wall
 * \param horizontal true if horizontal
 * \param endurance if -1 infinit endurance
 */
	Wall(int color, bool horizontal, int endurance = -1);
/**
 * \brief returns char representation of wall
 * \return wall symbol 
 */
	char getRepresentation() const;
/**
 * \brief returns behaviour NONE
 * \return NONE
 */
	int behaviour() const;
/**
 * \brief returns always false
 * \return false
 */
	bool shoot();
/**
 * \brief decrements endurance if not unbreakable
 * \return true
 */
	bool shot();
/**
 * \return false if unbreakable or endurance is higher than 0
 */
	bool isDead();
protected:
	bool horizontal; /**<true if wall is horizontal*/
	bool unbreakable; /**<true if there is no set endurance*/
	int endurance; /**<number of shots wall can stand*/
};

#endif