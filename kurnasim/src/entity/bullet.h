#ifndef BULLET
#define BULLET

#include "entity.h"

/**
 * \brief represents bullet being shot by shooters
 */

class Bullet : public Entity{
public:
/**
 * \brief initialises bullet entity
 * \param color color of entity, implicitly white
 */
	Bullet(int color = WHITE);
/**
 * \brief returns char represantation
 * \return symbol of bullet
 */
	char getRepresentation() const;
/**
 * \brief returns behaviour of the bullet
 * \return ATTACK
 */
	int behaviour() const;
/**
 * \brief returns true
 * \return always true
 */
	bool shoot();
/**
 * \brief returns always true so it will stop another bullet
 * \return true
 */
	bool shot();
/**
 * \brief returns always false
 * \return false
 */
	bool isDead();
};

#endif