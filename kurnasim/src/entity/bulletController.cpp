#include "bulletController.h"

BulletController::BulletController() {
}

BulletController::~BulletController(){
	for(auto i = activeBullet.begin(); i != activeBullet.end(); ++i)
		if((*i) != nullptr) delete (*i);
}

void BulletController::updatePosition(Map * map, ScoreController * score){
	int x, y, dirX, dirY;
	for(auto i = activeBullet.begin(); i != activeBullet.end(); ++i){
		if(*i != nullptr){
			(*i) -> getPosition(x, y);
			(*i) -> getDirection(dirX, dirY);

			if(map -> moveBullet(x, y, x + dirX, y + dirY, (*i) -> canShootTowers()))
				(*i) -> setPosition(x + dirX, y + dirY);
			else {
				delete (*i);
				(*i) = nullptr;
				Entity * obj = map -> getPosition(x + dirX, y + dirY);

				if(obj != nullptr)
					if(obj -> isDead()){
						if(obj -> behaviour() == ENEMY) (*score) ++;
						else (*score) --;
						map -> removePosition(x + dirX, y + dirY);
					}
			}
		}
	}
	removeDeactive();
}

void BulletController::addBullet(int x, int y, int dirX, int dirY, bool shootTowers){
	ShootPosition * shoot = new ShootPosition(x, y, dirX, dirY, shootTowers);
	activeBullet.push_back(shoot);
}

void BulletController::removeDeactive(){
	activeBullet.remove_if([](const ShootPosition * s){
		return s == nullptr;
	});
}