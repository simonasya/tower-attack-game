#ifndef GATE
#define GATE

#include "entity.h"

/**
 * \brief represents gate - entrance or exit
 */

class Gate : public Entity{
public:
/**
 * \brief initialises Entity of type Gate
 * \param color color of entity
 * \param exit true if presents exit, false if entrance
 */
	Gate(int color, bool exit);
/**
 * \brief returns symbol that represents gate on map
 * \return representation symbol
 */
	char getRepresentation() const;
/**
 * \brief returns behaviour
 * \return EXIT if exit true, if visitor not null representation of visitor
 * NONE in other cases
 */
	int behaviour() const;
/**
 * \brief returns always false as gate is incapable of shooting
 * \return false
 */
	bool shoot();
/**
 * \brief returns true, because it is capable of stopping bullet, but it does not affect it
 * \return true
 */
	bool shot();
/**
 * \return false
 */
	bool isDead();
protected:
	bool exit; /**<true if gate is exit, false if it is entrance*/
};

#endif