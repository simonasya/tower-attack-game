#include "gate.h"

Gate::Gate(int color, bool exit) : Entity(color), exit(exit) {}

char Gate::getRepresentation() const{
	if(visitor) return visitor -> getRepresentation();
	return GATE_REPRESENTATION;
}

int Gate::behaviour() const{
	if(exit) return EXIT;
	if(visitor) return visitor -> getRepresentation();
	return NONE;
}

bool Gate::shoot(){
	return false;
}

bool Gate::shot(){
	return true;
}

bool Gate::isDead(){
	return false;
}