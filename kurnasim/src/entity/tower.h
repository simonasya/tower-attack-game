#ifndef TOWER
#define TOWER

#include "shooter.h"

/**
 * \brief represents enemies of attackers in map
 */

class Tower : public Shooter{
public:
/**
 * \brief initialises instance of Tower
 * \param color color of entity
 * \param ammo number of ammunition
 * \param lives number of lives
 * \param x X position coordinate
 * \param y Y position coordinate
 */
	Tower(int color, int ammo, int lives, int x, int y);
/**
 * \brief rerturn tower map representation
 * \return char symbol
 */
	char getRepresentation() const;
/**
 * \brief returns ENEMY behvaiour
 * \return ENEMY value
 */
	int behaviour() const;
/**
 * \brief always returns true
 * \return true
 */
	bool shoot();
};

#endif