#ifndef SHOOTER
#define SHOOTER

#include "entity.h"

/**
 * \brief represents either tower or a attacker
 */

class Shooter : public Entity{
public:
/**
 * \brief initialises instance of shooter
 * \param color color of shooter
 * \param ammo number of ammunition
 * \param lives number of lives
 * \param x X coordinate of shooter
 * \param y Y coordinate of shooter
 */
	Shooter(int color, int ammo, int lives, int x, int y);
/**
 * \brief deletes object
 */
	virtual ~Shooter() {}	
/**
 * \brief returns shooter char representation in map
 * \return symbol of shooter
 */
	virtual char getRepresentation() const = 0;
/**
 * \brief returns shooter behavior
 * \details depends on shooter type
 * \return mapped int of entity behaviour
 */
	int behaviour() const = 0;
/**
 * \brief Decrements ammo
 * \return True, if is ammo available
 */
	virtual bool shoot();

/**
 * \brief Decrements lives
 * \return Always true
 */
	bool shot();

/**
 * \brief Check, if is shooter still alive
 * \return True if lives greater than zero, false otherwise
 */
	bool isDead();

/**
 * \brief Return count of ammo
 * \return Count of ammo
 */
	int getAmmo() const;

/**
 * \brief Return lives of attacker
 * \return Count of lives
 */
	int getLives() const;
protected:
	int ammo, lives;
	int x, y;
};

#endif