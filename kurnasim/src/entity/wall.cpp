#include "wall.h"

Wall::Wall(int color, bool horizontal, int endurance) 
: Entity(color), horizontal(horizontal), endurance(endurance) {
	if(endurance == -1)
		unbreakable = true;
	else unbreakable = false;
}

char Wall::getRepresentation() const{
	if(horizontal) return '#';
	return WALL_REPRESENTATION;
}

int Wall::behaviour() const{
	return NONE;
}

bool Wall::shoot(){
	return false;
}

bool Wall::shot(){
	if(unbreakable) return true;
	else endurance--;
	return true;
}

bool Wall::isDead(){
	if(unbreakable) return false;
	return endurance <= 0;
}
