#include "shootPosition.h"

ShootPosition::ShootPosition(int x, int y, int dirX, int dirY, bool shootTowers)
: x(x), y(y), dirX(dirX), dirY(dirY), shootTowers(shootTowers) {}

void ShootPosition::setPosition(int x, int y){
	this -> x = x;
	this -> y = y;
}

void ShootPosition::getPosition(int & x, int & y) const{
	x = this -> x;
	y = this -> y;
}

void ShootPosition::getDirection(int & dirX, int & dirY) const{
	dirX = this -> dirX;
	dirY = this -> dirY;
}

bool ShootPosition::canShootTowers() const{
	return shootTowers;
}