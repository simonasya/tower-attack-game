#ifndef ENTITY
#define ENTITY

#include <ncurses.h>
#include "../colors.h"
#include "../representation.h"

const int NONE = 0;
const int EXIT = 1;
const int SHOT = 2;
const int PATH = 3;
const int ENEMY = 4;
const int ATTACK = 5;

/**
 * \brief creates basics for map objects
 */

class Entity{
public:
/**
 * \brief initialises entity
 * \param color entity color
 */
	Entity(int color);
/**
 * \brief frees memory
 */
	virtual ~Entity();
/**
 * \brief changes color of entity
 * \param newColor new entity color
 */
	void setColor(int newColor);
/**
 * \brief returns current color of entity
 * \return number of color
 */
	int getColor() const;
/**
 * \brief returns pointer to visitor
 * \return entity object
 */
	Entity * getVisitor();
/**
 * \brief adds "visitor" - another entity that shares position in map
 * \param visitor entity object
 */
	void setVisitor(Entity * visitor);
/**
 * \brief returns char that will represent entity in the map
 * \return representation in one symbol form
 */
	virtual char getRepresentation() const = 0;
/**
 * \brief returns value that specifies how will the object behave in a situations
 * \return number mapped behaviour
 */
	virtual int behaviour() const = 0;
/**
 * \brief asks object to shoot if possible
 * \return true if the entity can shoot, false otherwise
 */
	virtual bool shoot() = 0;
/**
 * \brief called when there is bullet in the position of this entity
 * \return true if the bullet will stop, false otherwise
 */
	virtual bool shot() = 0;
/**
 * \brief returns information if the entity is killed
 * \return true if dead, false otherwise
 */
	virtual bool isDead() = 0;
protected:
	int color; /**<color of entity*/
	Entity * visitor; /**<entity sharing the same place in map*/
};

#endif