#include "entity.h"

Entity::Entity(int color) : color(color), visitor(nullptr) {}

Entity::~Entity(){
	if(visitor)
		delete visitor;
}

void Entity::setColor(int newColor){
	color = newColor;
}

int Entity::getColor() const{
	if(visitor) return visitor -> getColor();
	return color;
}

Entity * Entity::getVisitor(){
	return visitor;
}

void Entity::setVisitor(Entity * visitor){
	this -> visitor = visitor;
}
