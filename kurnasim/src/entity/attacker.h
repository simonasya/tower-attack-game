#ifndef ATTACKER
#define ATTACKER

#include "shooter.h"

/**
 * \brief represents attacker in map
 */

class Attacker : public Shooter{
public:
/**
 * \brief initialises attacker
 * \param color color of entity
 * \param ammo number of ammunition
 * \param lives number of lives
 * \param x X position coordinate
 * \param y Y position coordinate
 */
	Attacker(int color, int ammo, int lives, int x, int y);
/**
 * \brief returns attacker representative symbol
 * \return char of attacker
 */
	char getRepresentation() const;
/**
 * \brief returns ATTACK behaviour
 * \return ATTACK integer
 */
	int behaviour() const;
};

#endif