#include "attacker.h"

Attacker::Attacker(int color, int ammo, int lives, int x, int y) : Shooter(color, ammo, lives, x, y) {}

char Attacker::getRepresentation() const{
	return ATTACKER_REPRESENTATION;
}

int Attacker::behaviour() const{
	return ATTACK;
}