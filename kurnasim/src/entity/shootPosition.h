#ifndef SHOOT_POSITION
#define SHOOT_POSITION

/**
 * \brief holds information for bullet controller of one bullet
 */

class ShootPosition{
public:
/**
 * \brief creates new instance
 * \param x X starting position
 * \param y Y starting position
 * \param dirX X direction 
 * \param dirY Y direction
 * \param shootTowers true if capable of shooting towers
 */
	ShootPosition(int x, int y, int dirX, int dirY, bool shootTowers);
/**
 * \brief sets position of bullet
 * \param x X position
 * \param y Y position
 */
	void setPosition(int x, int y);
/**
 * \brief returns position
 * \param x X position
 * \param y Y position
 */
	void getPosition(int & x, int & y) const;
/**
 * \brief returns direction of bullet
* \param dirX X direction 
 * \param dirY Y direction
 */
	void getDirection(int & dirX, int & dirY) const;
/**
 * \return true if capable of shooting towers
 */
	bool canShootTowers() const;
private:
	int x, y, dirX, dirY;
	bool shootTowers;
};

#endif