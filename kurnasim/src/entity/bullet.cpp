#include "bullet.h"

Bullet::Bullet(int color) : Entity(color) {}

char Bullet::getRepresentation() const{
	return BULLET_REPRESENTATION;
}

int Bullet::behaviour() const{
	return ATTACK;
}

bool Bullet::shoot(){
	return true;
}

bool Bullet::shot(){
	return true;
}

bool Bullet::isDead(){
	return false;
}
