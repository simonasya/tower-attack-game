#ifndef PATH_ENTITY
#define PATH_ENTITY

#include "entity.h"

/**
 * \brief represents path in the map
 * \details path allows attackers walk through map
 */

class Path : public Entity{
public:
/**
 * \brief creates new instance of path
 * \param color color of path
 */
	Path(int color);
/**
 * \brief return path symbol representation
 * \return symbol
 */
	char getRepresentation() const;
/**
 * \brief returns behaviour of visitor if present, PATH otherwise
 * \return number of behaviour
 */
	int behaviour() const;
/**
 * \brief calls shoot() of visitor if present, false otherwise
 * \return bool value of shoot ability
 */
	bool shoot();
/**
 * \brief calls shot() of visitor if present, false otherwise
 * \return ability to being shot
 */
	bool shot();
/**
 * \brief returns isDead() of visitor if present, false otherwise
 * \return state bool value
 */
	bool isDead();	
};

#endif