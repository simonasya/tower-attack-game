#ifndef FILE_HANDLER
#define FILE_HANDLER

#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <stdio.h> 
using namespace std;

/**
 * \brief files handling class
 * \details allows reading and writing to file
 */

class File{
public:
/**
 * \brief file constructor
 * \param name file name
 */	 
	File(string name);
/**
 * \brief file destructor
 * \details closes currently opened file
 */
	~File();
/**
 * \brief opens file
 * \details sets cursor of reading and writing to begining
 * \return true if succesful, false otherwise
 */
	bool open();

/**
 * \brief reads from file in a form of <value name>delimer<value>
 * \details saves value name and value itself
 * \param valueName parsed name of value
 * \param value parsed value
 * \param delimer delimer of value name and value, if none provided, ':' used
 * \return true if there is a line in the correct form, false otherwise
 */
	bool parseFromFile(string & valueName, string & value, char delimer = ':');
/**
 * \brief opens and reads whole file
 * \param buffer file data
 * \return true if succesful, false otherwise
 */	
	bool readAll(string & buffer);
/**
 * \brief writes data to current cursor pointer in file
 * \details writes only given string, no additional data (newlines, etc.)
 * \param buffer data for writing
 * \return true if successful, false otherwise
 */
	bool write(string buffer);
/**
 * \brief opens file and adds data to the end
 * \param buffer data to write
 * \return true if successful, false otherwise
 */
	bool writeToEnd(string buffer);
/**
 * \brief checks for eof of file
 * \return true if encountered end of file, false otherwise
 */
	bool eof() const;
/**
 * \brief reads whole last line of the file
 * \param line value of line stored
 * \return true if successful, false otherwise
 */
	bool readLastLine(string & line);

/**
 * \brief reads value of last line in a format of <value name>delimer<value>
 * \return true if successful, false otherwise
 * \param valueName name of the value
 * \param value where the value will be stored
 * \param delimer defaultly ':', divider of valueName and value
 */
	bool parseLastLine(string & valueName, string & value, char delimer = ':');
private:
	fstream file; /**<file stream*/
	string name; /**<name of the file*/
	int position; /**<position in the file*/
};

#endif