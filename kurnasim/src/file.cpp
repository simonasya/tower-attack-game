#include "file.h"

File::File(string name) : name(name), position(0) {}

File::~File(){
	file.close();
}

bool File::open(){
	if(file.is_open()) 
		return true;
	file.open(name, fstream::in | fstream::out);
	if(!file.is_open())
		return false;
	file.seekp(0);
	file.seekg(0);
	position = 0;
	return true;
}

bool File::parseFromFile(string & valueName, string & value, char delimer){
	if(!file.is_open()) 
		open();
	getline(file, valueName, delimer);
	getline(file, value);
	return !file.fail();
}

bool File::readAll(string & buffer){
	if(!file.is_open()) 
		open();
	file.seekg(0);
	stringstream s;
	s << file.rdbuf();
	buffer = s.str();
	return !file.fail();
}

bool File::write(string buffer){
	file << buffer;
	return file.good();
}

bool File::writeToEnd(string buffer){
	file.open(name, fstream::in | fstream::out | fstream::app);
	file << buffer;
	return file.good();
}

bool File::eof() const{
	return file.eof();
}

bool File::readLastLine(string & line){
	if(!open()) 
		return false;

    while (file >> std::ws 
    	&& getline(file, line)){}
    return file.good();
}

bool File::parseLastLine(string & valueName, string & value, char delimer){
	if(!open()) 
		return false;

	 while (file >> std::ws 
	 	&& getline(file, valueName, delimer)
	 	&& getline(file, value)){}
    return file.good();
}