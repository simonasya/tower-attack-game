#ifndef EXCEPTION_HANDLER
#define EXCEPTION_HANDLER

#include <exception>
#include <string>
using namespace std;	

const string FILE_OPEN_ERROR = "No such file or invalid permissions";
const string FILE_WRITE_ERROR = "Error while writing to file";
const string FILE_READ_ERROR = "Error reading from file";

/**
 * \brief handles exception 
 * \details inherits from std::exception
 */

class ExceptionHandler : public exception{
public:
/**
 * \brief creates new exception
 * \param error text describing error
 */
	ExceptionHandler(const string & error);
/**
 * \brief creates new exception
 * \param obj object in a string form that is causing the exception
 * \param error text describing error
 */
	ExceptionHandler(const string & obj, const string & error);
/**
 * \brief returns value of error
 * \return error description
 */
	const char * what () const throw();
private:
	string error; /**<error description*/
};

#endif