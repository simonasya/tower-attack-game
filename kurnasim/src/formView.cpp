#include "formView.h"

FormView::FormView() : formWindow(nullptr) {}

FormView::~FormView(){
	deleteFormBox();
}

void FormView::show(string title, string request){
	if(!formWindow)
		formWindow = newwin(FORM_HEIGHT, FORM_WIDTH, 1, 1);

	wattron(formWindow, COLOR_PAIR(WHITE));
	mvwprintw(formWindow, 1,1, "%s", title.c_str());
	mvwprintw(formWindow, 3, 1, "%s", request.c_str());
	wattroff(formWindow, COLOR_PAIR(WHITE));
	
	echo();
	curs_set(true);
	move(4, 3 + request.length());

	wrefresh(formWindow);

	char input[100];
	wgetstr(formWindow, input);
	parsed = (string)input;
	wrefresh(formWindow);
	curs_set(false);	
	noecho();
}

void FormView::deleteFormBox(){
	if(formWindow)
		deleteBox(formWindow);
}

string FormView::getParsed() const{
	return parsed;
}