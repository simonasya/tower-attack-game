#include "menuController.h"

MenuController::MenuController(string title, const vector<string> & item) 
: menu(new Menu(title, item)), menuView(new MenuView()) {}

MenuController::~MenuController(){
	delete menu;
	delete menuView;
}

void MenuController::show(){
	menuView -> show(menu);
}

int MenuController::takeInput(WindowController * win){
	return menuView -> takeInput(menu, win);
}

void MenuController::deleteMenuBox(){
	menuView -> deleteMenuBox();
}

void MenuController::setPadding(int x, int y){
	menuView -> setPadding(x, y);
}