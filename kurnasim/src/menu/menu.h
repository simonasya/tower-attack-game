#ifndef MENU
#define MENU

#include <vector>
#include <string>
using namespace std;

/**
 * items of default start menu
 */
const vector<string> MENU_ITEMS = { 
	"Play", "Continue", "Highest Score", "Tutorial", "Exit" 
};

/**
 * items of menu after player loses level
 */
const vector<string> LOSE_MENU_ITEMS = {
	"Try again", "Save game", "Exit"
};

/**
 * items of menu after player wins level
 */
const vector<string> WIN_MENU_ITEMS = {
	"Next level", "Save game", "Exit"
};

/**
 * \brief represents menu, its items and current position
 */

class Menu{
public:
/**
 * \brief initialise instance of menu
 * \param title text of title
 * \param item vector of menu items
 * \param defaultPosition index of item that presents default position
 */
	Menu(string title, const vector<string> & item, int defaultPosition = 0);
/**
 * \brief returns size menu item vector
 * \return number of menu items
 */	
	unsigned int size() const;
/**
 * \brief moves up in menu
 */
	void moveUp();
/**
 * \brief moves down in menu
 */
	void moveDown();
/**
 * \brief returns current position
 * \return index of currently active item
 */
	int getPosition() const;
/**
 * \brief returns menu title text
 * \return title
 */
	string getTitle() const;
/**
 * \brief returns menu item
 * \param index index of item
 * \return item of given index
 */
	string getItem(int index) const;
private:
	string title; /**<title of menu*/
	vector<string> item; /**<list of menu items*/
	int position; /**<current position, index of item*/
}; 

#endif