#include "menuView.h"

MenuView::MenuView() : menuBox(nullptr), paddingX(1), paddingY(1) {}

MenuView::~MenuView(){
	deleteMenuBox();
}

void MenuView::moveUp(Menu * menu){
	menu -> moveUp();
	show(menu);
}

void MenuView::moveDown(Menu * menu){
	menu -> moveDown();
	show(menu);
}

void MenuView::show(Menu * menu){
	if(!menuBox) 
		menuBox = newwin(menu -> size() + 2, WIN_WIDTH, paddingX, paddingY);
	
	wattron(menuBox, COLOR_PAIR(CYAN));
	wattroff(menuBox, A_STANDOUT);
	mvwprintw(menuBox, 0, 1, "%s", menu -> getTitle().c_str());
	wattroff(menuBox, COLOR_PAIR(CYAN));

	for(unsigned int i = 0; i < menu -> size(); i++){
		if((int)i == menu -> getPosition()) 
			wattron(menuBox, A_STANDOUT);
		else wattroff(menuBox, A_STANDOUT);
		mvwprintw(menuBox, i + 2, 1, "%s", menu -> getItem(i).c_str());
	}
	wrefresh(menuBox);
}

int MenuView::takeInput(Menu * menu, WindowController * win){
	while(true){
		show(menu);
		timeout(MENU_INPUT_TIMEOUT);
		int key = wgetch(win -> getWindow());
		switch(key){
			case KEY_ALT_ESC: return -1;
			case KEY_ENTER: case KEY_ENTER1: case KEY_ENTER2: 
				return menu -> getPosition();
			case KEY_UP: moveUp(menu);
				break;
			case KEY_DOWN: moveDown(menu);
				break;
		}
	}
}

void MenuView::deleteMenuBox(){
	if(menuBox)
		deleteBox(menuBox);
	menuBox = nullptr;
}

void MenuView::setPadding(int x, int y){
	paddingX = x;
	paddingY = y;
}
