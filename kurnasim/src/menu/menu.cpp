#include "menu.h"

Menu::Menu(string title, const vector<string> & item, int defaultPosition) 
: title(title), item(item), position(defaultPosition) {}

unsigned int Menu::size() const {
	return item.size();
}

void Menu::moveUp(){
	position --;
	if(position < 0) 
		position = (int)item.size() - 1;
}

void Menu::moveDown(){
	position ++;
	if(position >= (int)item.size()) 
		position = 0;
}

int Menu::getPosition() const{
	return position;
}

string Menu::getTitle() const{
	return title;
}

string Menu::getItem(int index) const{
	return item[index];
}