#ifndef MENU_VIEW
#define MENU_VIEW

#include "menu.h"
#include "../functions.h"
#include "../windowController.h"
#include "../keys.h"

const double MENU_INPUT_TIMEOUT = 0.01; /**<timeout of one cycle of takeinput()*/

/**
 * \brief creates window and user interface of menu
 */

class MenuView {
public:
/**
 * \brief initialises instance of menuView
 * \details sets implicitly padding to (1,1)
 */
	MenuView();
/**
 * \brief frees resources
 */
	~MenuView();
/**
 * \brief is called when user presses UP key
 * \details moves position in menu up
 * \param menu menu instance
 */
	void moveUp(Menu * menu);
/**
 * \brief is called when user presses DOWN key
 * \details moves position in menu down
 * \param menu menu instance
 */
	void moveDown(Menu * menu);
/**
 * \brief shows menu
 * \param menu menu to show - provides resource for menu items
 */
	void show(Menu * menu);
/**
 * \brief shows menu and takes user input
 * \param menu menu to show
 * \param win main window
 * \return index of chosen item after ENTER
 */
	int takeInput(Menu * menu, WindowController * win);
/**
 * \brief deletes and clears subwindow of menu
 */
	void deleteMenuBox();
/**
 * \brief sets position of the menu
 * \details position is X and Y coordinate of left upper corner of the menu
 * \param x X coordinate
 * \param y Y coordinate
 */
	void setPadding(int x, int y);
private:
	WINDOW * menuBox; /**<subwindow for menu*/
	int paddingX; /**<paddinx X coordinate*/ 
	int paddingY; /**<paddinx Y coordinate*/ 
};

#endif