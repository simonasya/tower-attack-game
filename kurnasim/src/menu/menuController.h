#ifndef MENU_CONTROLLER
#define MENU_CONTROLLER

#include "menuView.h"
#include "menu.h"

/**
 * \brief creates simplyfied interface for menu and its view
 */

class MenuController{
public:
/**
 * \brief initialises instance of MenuController
 * \details creates new menu and its view
 * \param title title before the menu
 * \param item vector of items
 */
	MenuController(string title, const vector<string> & item);
/**
 * \brief frees memory
 */
	~MenuController();
/**
 * \brief calls view show() method
 */
	void show();
/**
 * \brief waits for user input
 * \details if any item was chosen, returns chosen value
 * \param win currently used window in a form of windowController
 * \return index of chosen item
 */
	int takeInput(WindowController * win);
/**
 * \brief deletes subwindow of menu
 * \details is called by destructor implicitly
 */
	void deleteMenuBox();
/**
 * \brief sets padding of left upper corner of the menu
 * \param x X coordinate
 * \param y Y coordinate
 */
	void setPadding(int x, int y);
private:
	Menu * menu; /**<instance of menu*/
	MenuView * menuView; /**<instance of view*/
};

#endif
