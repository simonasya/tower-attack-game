#include "game.h"

Game::Game() 
: windowController(new WindowController()), menu(nullptr) {
	currentLevel.map = nullptr;
	currentLevel.index = 0;
	points = 0;
	maxPoints = 0;
}

Game::~Game(){
	if(windowController) 
		delete windowController;
	if(menu) 
		delete menu;
	if(currentLevel.map) 
		delete currentLevel.map;
}

void Game::loadConfig(){
	File * file = new File(CONF_FILE);
	string valueName, value;

	if(!file -> open())
		throw ExceptionHandler(CONF_FILE, FILE_OPEN_ERROR);

	while(!file -> eof()){
		file -> parseFromFile(valueName, value);
		if(!valueName.empty())
			configuration[valueName] = stoi(value);
	}
	delete file;
	getHighestScore();
}

void Game::showMenu(){
	while(true){
		menu = new MenuController(MENU_TITLE, MENU_ITEMS);
		menu -> show();
		int chosen = menu -> takeInput(windowController);
		delete menu;
		menu = nullptr;

		if(chosen == 0) {
			currentLevel.index = 0;
			points = 0;
			play();
		}
		if(chosen == 1) showSaved();
		if(chosen == 2) showScore();
		if(chosen == 3) showPage(TUTORIAL_PAGE_TITLE, TUTORIAL_FILE);
		if(chosen == 4) return;
	}
}

void Game::showPage(const string & title, const string & fileName){
	string text;
	File * file = new File(fileName);

	if(!file -> open())
		throw ExceptionHandler(fileName, FILE_OPEN_ERROR);

	file -> readAll(text);
	delete file;
		
	PageController * page = new PageController(title, text, GREEN, WHITE);
	page -> setPageSize(WIN_WIDTH, WIN_HEIGHT);
	page -> takeInput(windowController);
	delete page;
}

void Game::play(){
	if(currentLevel.index == 0) 
		currentLevel.index++;

	while(loadLevel()){
		int status = startLevel();
		MenuController * levelMenu = nullptr;

		if(status == -1) {
			delete currentLevel.map;
			currentLevel.map = nullptr;
			saveLevel();
			return;
		}

		if(status == 0)
			levelMenu = new MenuController(LOSE_MENU_TITLE, LOSE_MENU_ITEMS);

		if(status == 1){
			currentLevel.index++;
			points += currentLevel.map -> Points();
			levelMenu = new MenuController(WIN_MENU_TITLE + to_string(points), WIN_MENU_ITEMS);
		}
		
		delete currentLevel.map;
		currentLevel.map = nullptr;
		
		int choice = levelMenu -> takeInput(windowController);
		delete levelMenu;
		
		switch(choice){
			case 0: break; 
			case 1: saveLevel();
					return;
			case 2: if(maxPoints < points) saveScore();
					return;
		}
	}
}

bool Game::loadLevel(){
	if(currentLevel.index > configuration["levels"]) return false;
	string levelFile = LEVEL_PATH + to_string(currentLevel.index); 

	currentLevel.map = new MapController();
	currentLevel.map -> loadMap(levelFile);
	return true;
}

int Game::startLevel(){
	string title = "Level: " + to_string(currentLevel.index);
	return currentLevel.map -> show(title);
}

void Game::saveLevel(){
	string username = getUsername("Saving game"); 
	if(username.empty()) 
		return; 

	File * file = new File(SAVED_GAMES_FILE);
	if(!file -> writeToEnd(username + ":" + to_string(currentLevel.index) + " " + to_string(points) + "\n"))
		throw ExceptionHandler(SAVED_GAMES_FILE, FILE_WRITE_ERROR);
	delete file;

	if(maxPoints < points)
		saveScore(username);
}

void Game::saveScore(){
	string username = getUsername("New highest score: " + to_string(points));
	if(username.empty())
		return; 
	saveScore(username);
}

void Game::saveScore(const string & username){
	File * file = new File(SCORE_FILE);
	if(!file -> writeToEnd(username + ":" + to_string(points) + "\n"))
		throw ExceptionHandler(SCORE_FILE, FILE_WRITE_ERROR);
	delete file;
	maxPoints = points;
}

string Game::getUsername(const string & request){
	Form * form = new Form(request, "Your name: ");
	form -> show();
	string username = form -> getParsed();
	delete form;
	return username;
}

void Game::showScore(){
	File * file = new File(SCORE_FILE);
	string scoreList, username, userPoints;
	
	if(!file -> open())
		throw ExceptionHandler(SCORE_FILE, FILE_OPEN_ERROR);

	while(!file -> eof()){
		file -> parseFromFile(username, userPoints);
		if(!username.empty())
			scoreList.insert(0, userPoints + " [" + username + "]\n");
	}
	delete file;
	scoreList.insert(0, "\n");

	PageController * page = new PageController(SCORE_PAGE_TITLE, scoreList, CYAN, WHITE);
	page -> setPageSize(WIN_WIDTH, WIN_HEIGHT);
	page -> takeInput(windowController);
	delete page;
}

void Game::getHighestScore(){
	string value, valueName;
	File * file = new File(SCORE_FILE);
	file -> parseLastLine(valueName, value);

	if(value.empty()){
		maxPoints = 0;
		delete file;
		return;
	}
	maxPoints = stoi(value);
	delete file;
}

void Game::showSaved(){
	vector<string> name;
	vector<int> level, point;
	parseSaved(name, level, point);

	if(name.size() == 0){
		PageController * page = new PageController(SAVED_MENU_TITLE, "(no saved games)", CYAN, BLACK);
		page -> setPageSize(WIN_WIDTH, WIN_HEIGHT);
		page -> takeInput(windowController);
		delete page;
		return;
	}

	MenuController * savedMenu = new MenuController(SAVED_MENU_TITLE, name);
	int choice = savedMenu -> takeInput(windowController);
	delete savedMenu;

	if(choice == -1) 
		return;

	if(choice >= 0 && choice < (int)name.size()){
		currentLevel.index = level[choice];
		points = point[choice];
		play();
	}
}

void Game::parseSaved(vector<string> & name, vector<int> & level, vector<int> & point) const{
	File * file = new File(SAVED_GAMES_FILE);
	string valueName, value;
	
	if(!file -> open())
		throw ExceptionHandler(SAVED_GAMES_FILE, FILE_OPEN_ERROR);

	while(!file -> eof()){
		file -> parseFromFile(valueName, value);
		if(!valueName.empty()){
			name.insert(name.end(), valueName);
			string tmp = value.substr(0, value.find(" "));
			level.insert(level.end(), stoi(tmp));
			point.insert(point.end(), stoi(value.substr(tmp.size())));
		}
	}
	delete file;
}