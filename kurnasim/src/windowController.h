#ifndef WINDOW_CONTROLLER
#define WINDOW_CONTROLLER

#include <ncurses.h>
#include "colors.h"

const int WIN_HEIGHT = 200; /**<window height*/
const int WIN_WIDTH = 300; /**<window width*/

/**
 * \brief creates and stores main window
 */

class WindowController{
public:
/**
 * \brief creates new window
 * \details initialises ncurses mode, colors, cursor and keys
 */
	WindowController();
/**
 * \brief closes and deletes window
 */
	~WindowController();
/**
 * \brief returns main window to view classes
 * \return main window
 */
	WINDOW * getWindow();
private:
	WINDOW * window; /**<pointer to main window*/
};

#endif

