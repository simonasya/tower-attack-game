#include "exceptionHandler.h"

ExceptionHandler::ExceptionHandler(const string & error) : error(error) {}

ExceptionHandler::ExceptionHandler(const string & obj, const string & error){
	this -> error = obj + " : " + error;
}

const char * ExceptionHandler::what () const throw() {
	return error.c_str();
}