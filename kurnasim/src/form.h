#ifndef FORM
#define FORM
#include <string>
#include "formView.h"
using namespace std;

/**
 * \brief stores form information and via formView creates GUI for user input
 */

class Form{
public:
/**
 * \brief creates new form instance
 * \param title title of form
 * \param request name of the value for user to give
 */
	Form(const string & title, const string & request);
/**
 * \brief frees memory used by class
 */
	~Form();
/**
 * \return title of the form
 */
	string getTitle();
/**
 * \return form request
 */
	string getRequest();
/**
 * \brief shows the form itself
 * \details calls formView show() method
 */
	void show();
/**
 * \brief deletes subwindow for form
 */
	void deleteFormBox();
/**
 * \brief possible to call only after input from user
 * \return value of user input
 */
	string getParsed() const;
private:
	string title; /**<title of form*/
	string request; /**<request for user*/
	FormView * formView; /**<interface*/
};

#endif