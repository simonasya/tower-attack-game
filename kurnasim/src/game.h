#ifndef GAME
#define GAME

#include <ncurses.h>
#include <iostream>
#include <map>

#include "menu/menuController.h"
#include "map/mapController.h"
#include "score/scoreController.h"
#include "windowController.h"
#include "page/pageController.h"
#include "form.h"
#include "colors.h"
#include "file.h"
#include "exceptionHandler.h"

using namespace std;

const string CONF_FILE = "src/source/config";
const string LOG_FILE = "src/source/log";
const string SCORE_FILE = "src/source/score";
const string TUTORIAL_FILE = "src/source/tutorial";
const string SAVED_GAMES_FILE = "src/source/saved";
const string LEVEL_PATH = "src/source/levels/";

const string SCORE_PAGE_TITLE = "HIGHEST SCORE";
const string SAVED_PAGE_TITLE = "SAVED GAMES";
const string TUTORIAL_PAGE_TITLE = "TUTORIAL";

const string MENU_TITLE = "TOWER ATTACK";
const string LOSE_MENU_TITLE = "YOU LOST!";
const string WIN_MENU_TITLE = "YOU WON! Your points: ";
const string SAVED_MENU_TITLE = "SAVED GAMES";

/**
 * \brief represents whole game and includes controll methods
 */

class Game{
public:
/**
 * \brief initialises instance of Game
 * \details sets current level to 0, creates new WindowController and score counter
 */
	Game();
/**
 * \brief delete member values
 */
	~Game();
/**
 * \brief loads data from configuration file
 */
	void loadConfig();
/**
 * \brief shows menu and waits for input
 * \details based on input sends to the right option
 */
	void showMenu();

private:
/**
 * \brief initializes instance of Page 
 * \details loads page contents from given file
 * \param title title for the page
 * \param fileName name of file to load page from
 */
	void showPage(const string & title, const string & fileName);
/**
 * \brief initialises level and waits for result
 * \details at the end of level shows menu (for win or loss)
 */
	void play();
/**
 * \brief loads level from file and sets values
 * \details initialise map and score
 * \return true if there is level to load, false otherwise
 */
	bool loadLevel();
/**
 * \brief starts loaded level
 * \return 1 if won, 0 if lost, -1 after exit
 */
	int startLevel();
/**
 * \brief saves current level and name of a player to file
 */
	void saveLevel();
/**
 * \brief saves score if current best to file
 */
	void saveScore();
/**
 * \brief saves score and username to file
 * \details called from saveScore()
 * \param usename name of the player
 */
	void saveScore(const string & usename);
/**
 * \brief asks player for username and returns it as string
 * \param request request text for username
 * \return username of player
 */
	string getUsername(const string & request);
/**
 * \brief shows page with current highest scores
 * \details updates throughout the game
 */
	void showScore();
/**
 * \brief reads highest score from file
 * \details updates variable maxPoints
 */
	void getHighestScore();
/**
 * \brief shows menu of saved games
 * \details player can choose which saved game he wants to continue playing
 */
	void showSaved();
/**
 * \brief reads from game saved file and parse it to vektors
 * \param name username of player
 * \param level unfinished level
 * \param point number of achieved points
 */
	void parseSaved(vector<string> & name, vector<int> & level, vector<int> & point) const;

	WindowController * windowController; /**<manager for main window*/
	MenuController * menu; /**<main menu*/
	int points; /**<points of currently played game*/
	int maxPoints; /**<current highest score*/

	struct CurrentLevel{
		MapController * map; /**<map of current level*/
		int index; /**<index of level*/
	}; CurrentLevel currentLevel;

	map<string, int> configuration; /**<configuration of game*/
};

#endif