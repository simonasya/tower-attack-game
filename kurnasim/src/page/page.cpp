#include "page.h"

Page::Page(const string & title, const string & text, int titleColor, int textColor) 
: title(title), text(text), titleColor(titleColor), textColor(textColor) {}

void Page::setPageSize(int width, int height){
	this -> width = width;
	this -> height = height;
}

void Page::getPageSize(int & width, int & height) const{
	width = this -> width;
	height = this -> height;
}

string Page::getTitle() const{
	return title;
}

string Page::getText() const{
	return text;
}

int Page::getTitleColor() const{
	return titleColor;
}

int Page::getTextColor() const{
	return textColor;
}