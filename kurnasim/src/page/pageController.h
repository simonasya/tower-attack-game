#ifndef PAGE_CONTROLLER
#define PAGE_CONTROLLER

#include "page.h"
#include "pageView.h"

/**
 * \brief controlls page data and pageview and provides interface to create and show data
 */

class PageController{
public:
/**
 * \brief creates instance of controller
 * \param title title of page to be displayed
 * \param text text to be displayed
 * \param titleColor color of the title
 * \param textColor color of the text
 */
	PageController(string title, string text, int titleColor, int textColor);
/**
 * \brief deletes data and closes subwindow
 */	
	~PageController();
/**
 * \brief sets size of the subwindow
 * \param width width of subwindow
 * \param height height of subwindow
 */
	void setPageSize(int width, int height);
/**
 * \brief calls show() of the view
 */
	void show();
/**
 * \brief calls takeInput() of the view
 * \param windowController controller of the main window
 */
	int takeInput(WindowController * windowController);
/**
 * \brief deletes subwindow of the page
 */
	void deletePageWindow();
private:
	Page * page; /**<instance of page providing data to display*/
	PageView pageView; /**<instance of view to diplay data*/
};

#endif