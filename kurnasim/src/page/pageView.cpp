#include "pageView.h"

PageView::PageView() : pageWindow(nullptr) {}

PageView::~PageView(){
	deletePageWindow();
}

void PageView::show(const Page * page) {
	int height, width;
	page -> getPageSize(width, height);

	if(!pageWindow)
		pageWindow = newwin(height, width, 1, 1);
	writeToWindow(pageWindow, page -> getTitle(), 0, 0, page -> getTitleColor());
	if(!page -> getText().empty())
		writeToWindow(pageWindow, page -> getText(), 0, 1, page -> getTextColor());
	wrefresh(pageWindow);
}

int PageView::takeInput(const Page * page, WindowController * windowController) {
	int key = -1;
	while(key != KEY_BACKSPACE && key != KEY_ALT_ESC){
		show(page);
		timeout(0.01);
		key = wgetch(pageWindow);
	}
	return key;
}

void PageView::deletePageWindow(){
	if(pageWindow)
		deleteBox(pageWindow);
	pageWindow = nullptr;
}
