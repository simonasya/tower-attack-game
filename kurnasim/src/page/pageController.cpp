#include "pageController.h"

PageController::PageController(string title, string text, int titleColor, int textColor) 
: page(new Page(title, text, titleColor, textColor)) {}

PageController::~PageController(){
	if(page) delete page;
}

void PageController::setPageSize(int width, int height){
	page -> setPageSize(width, height);
}

void PageController::show(){
	pageView.show(page);
}

int PageController::takeInput(WindowController * windowController){
	return pageView.takeInput(page, windowController);
}

void PageController::deletePageWindow(){
	pageView.deletePageWindow();
}