#ifndef PAGE
#define PAGE

#include <vector>
using namespace std;
#include "../menu/menuController.h"

/**
 * \brief stores data to be shown in page view
 */

class Page{
public:
/**
 * \brief intialises instance of page
 * \param title title text of shown page
 * \param text the rest of text on page
 * \param titleColor color of title
 * \param textColor color of text
 */
	Page(const string & title, const string & text, int titleColor, int textColor);
/**
 * \brief sets page size
 * \details is it necessary to specify
 * \param width width of page subwindow
 * \param height height of page subwindow
 */
	void setPageSize(int width, int height);
/**
 * \brief returns page subwindow size
 * \param width width of page subwindow
 * \param height height of page subwindow
 */
	void getPageSize(int & width, int & height) const;
/**
 * \brief returns title of page
 * \return page title
 */
	string getTitle() const;
/**
 * \brief returns text of page
 * \return page text
 */
	string getText() const;
/**
 * \brief returns color of the title
 * \return title color
 */
	int getTitleColor() const;
/**
 * \brief returns color of the text
 * \return text color
 */
	int getTextColor() const;
private:
	string title; /**<title of page*/
	string text; /**<text ofpage*/
	int titleColor; /**<title color*/
	int textColor; /**<text color*/
	int width; /**<subwindow width*/
	int height; /**<subwindow height*/
};

#endif