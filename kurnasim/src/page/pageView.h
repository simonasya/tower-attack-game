#ifndef PAGE_VIEW
#define PAGE_VIEW

#include <ncurses.h>
#include "page.h"
#include "../functions.h"

/**
 * \brief creates user interface for page
 */

class PageView{
public:
/**
 * \brief creates page view instance
 */
	PageView();
/**
 * \brief frees memory
 * \details destroyes and closes subwindow
 */
	~PageView();
/**
 * \brief shows page with title and text
 * \param page resource for data to show
 */
	void show(const Page * page);
/** 
 * \brief calls show() and waits for ESC to close
 * \param windowController main window controller
 * \return input
 */
	int takeInput(const Page * page, WindowController * windowController);
/**
 * \brief closes and deletes page subwindow
 */
	void deletePageWindow();
private:
	WINDOW * pageWindow; /**<subwindow for page to be displayed on*/
};

#endif
