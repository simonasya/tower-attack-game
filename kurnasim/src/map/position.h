#ifndef POSITION
#define POSITION

/**
 * \brief class for storing BFS algorithm information during shortest path search
 * \details stores position and its ancestor
 * 
 */

class Position{
public:
/**
 * \brief initialises instance of position
 * \param x coordinate X
 * \param y coordinate Y
 */
	Position(int x, int y);
/**
 * \brief returns current position
 * \param x coordinate X 
 * \param y coordinate Y
 */
	void getPosition(int & x, int & y) const;
/**
 * \brief returns ancestor of this position
 * \details used in graph algorithm
 * \param x coordinate X 
 * \param y coordinate Y
 */
	void getAncestor(int & x, int & y) const;
/**
 * \brief sets ancestor of current position
 * \param ancestorX coordinate X of ancestor
 * \param ancestorY coordinate Y of ancestor
 */
	void setAncestor(int ancestorX, int ancestorY);
private:
	int x; /**<current X coordinate*/
	int y; /**<current Y coordinate*/
	int ancestorX; /**<X coordinate of ancestor*/
	int ancestorY; /**<Y coordinate of ancestor*/
};

#endif