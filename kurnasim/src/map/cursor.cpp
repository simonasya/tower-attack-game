#include "cursor.h"

Cursor::Cursor() : directionX(1), directionY(0), currentEntrance(0) {}

void Cursor::setCurrentPosition(int x, int y){
	this -> x = x;
	this -> y = y;
}

void Cursor::getCurrentPosition(int & x, int & y) const{
	x = this -> x;
	y = this -> y;
}

void Cursor::setShootDirection(int x, int y){
	directionX = x;
	directionY = y;
}

void Cursor::getShootDirection(int & x, int & y) const{
	x = directionX;
	y = directionY;
}

void Cursor::setEntrance(int x, int y){
	entranceX.push_back(x);
	entranceY.push_back(y);
}

void Cursor::placeAttacker(ScoreController * score, Map * map, int ammo, int lives){
	x = entranceX[currentEntrance];
	y = entranceY[currentEntrance++];
	
	if(currentEntrance >= (int)entranceX.size())
		currentEntrance = 0;
	
	Attacker * attacker = map -> placeAttacker(x, y, ammo, lives);
	score -> setCurrentAttacker(attacker);
}

bool Cursor::moveLeft(Map * map){
	return move(map, x - 1, y);
}

bool Cursor::moveRight(Map * map){
	return move(map, x + 1, y);
}

bool Cursor::moveUp(Map * map){
	return move(map, x, y - 1);
}

bool Cursor::moveDown(Map * map){
	return move(map, x, y + 1);
}

bool Cursor::attackerShot(Map * map) const{
	return !map -> getPosition(x, y) || !map -> getPosition(x, y) -> getVisitor();
}

bool Cursor::attackerInExit(Map * map){
	return map -> getPosition(x, y) && map -> getPosition(x, y) -> behaviour() == EXIT;
}

bool Cursor::move(Map * map, int toX, int toY){
	if(map -> canPlaceAttacker(toX, toY)){
		map -> move(x, y, toX, toY);
		x = toX;
		y = toY;
		return true;
	}
	return false;
}

void Cursor::cleanEntrances(){
	entranceX.clear();
	entranceY.clear();
}
