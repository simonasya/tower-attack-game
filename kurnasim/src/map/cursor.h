#ifndef CURSOR
#define CURSOR

#include "map.h"
#include "../score/scoreController.h"

/**
 * \brief represents current position in the map and player 
 */

class Cursor{
public:
/**
 * \brief initialises new empty instance
 */
	Cursor();
/**
 * \brief sets current position of the players attacker
 * \param x X coordinate
 * \param y Y coordinate
 */
	void setCurrentPosition(int x, int y);
/**
 * \brief returns current position of the players attacker
 * \param x X coordinate
 * \param y Y coordinate
 */
	void getCurrentPosition(int & x, int & y) const;
/**
 * \brief sets current current shoot direction of the attacker
 * \param x X direction
 * \param y Y direction
 */
	void setShootDirection(int x, int y);
/**
 * \brief retuns current current shoot direction of the attacker
 * \param x X direction
 * \param y Y direction
 */
	void getShootDirection(int & x, int & y) const;
/**
 * \brief adds entrance position to list
 * \param x X coordinate
 * \param y Y coordinate
 */
	void setEntrance(int x, int y);
/**
 * \brief places new instance of attacker to start position (one of the entrances)
 * \details picks entrances in the ascending order
 * \param score score controller of the level
 * \param map current map
 * \param ammo ammunition of the new attacker
 * \param lives number of lives  of the new attacker
 */
	void placeAttacker(ScoreController * score, Map * map, int ammo, int lives);
/**
 * \brief moves position to left
 * \param map current map
 * \return true if movement possible
 */	
	bool moveLeft(Map * map);
/**
 * \brief moves position to right
 * \param map current map
 * \return true if movement possible
 */	
	bool moveRight(Map * map);
/**
 * \brief moves position to up
 * \param map current map
 * \return true if movement possible
 */	
	bool moveUp(Map * map);
/**
 * \brief moves position to down
 * \param map current map
 * \return true if movement possible
 */	
	bool moveDown(Map * map);
/**
 * \brief returns true if current attacker shot
 * \param map current map
 * \return true if shot, false otherwise
 */
	bool attackerShot(Map * map) const;
/**
 * \brief returns true if current attacker in the exit
 * \param map current map
 * \return true if int the exit position, false otherwise
 */
	bool attackerInExit(Map * map);
/**
 * \brief removes attacker from the exit 
 */
	void cleanEntrances();
private:
/**
 * \brief moves to new position
 * \param map current map
 * \param toX new X coordinate
 * \param toY new Y coordinate
 * \return true if movement possible
 */
	bool move(Map * map, int toX, int toY);
	int x;
	int y;
	int directionX;
	int directionY; 
	vector<int> entranceX; /**<X coordinate of entrance*/
	vector<int> entranceY; /**<Y coordinate of entrance*/
	int currentEntrance; /**<number of current entrance for attacker to use*/
};

#endif