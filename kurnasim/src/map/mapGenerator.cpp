#include "mapGenerator.h"

MapGenerator::MapGenerator(int width, int height, Map * map)
: width(width), height(height), map(map) {}

void MapGenerator::generateWalls(int count) const{
	int placed = 0, x, y;
	while(placed < count){
		x = getRandomInt(0, width - 1);
		y = getRandomInt(0, height - 1);
		if(map -> canPlace(x, y)){
			map -> placeWall(x, y);
			placed ++;
		}
	}
}

bool MapGenerator::generateGates(int entranceCount, Cursor * cursor) const{
	bool placed = false;
	int x, y;

	while(!placed){
		if(getRandomBool()){
			x = getRandomInt(0, width - 1);
			if(getRandomBool()) y = height - 1;
			else y = 0;
		}
		else {
			y = getRandomInt(0, height - 1);
			if(getRandomBool()) x = width - 1;
			else x = 0;
		}
		if(map -> canPlaceGate(x, y)){
			map -> placeExit(x, y);
			placed = true;
		}
	}
	return generateEntrances(x, y, entranceCount, cursor);
}

void MapGenerator::generateTowers(int count, int ammo, int lives, vector<int> & towerX, vector<int> & towerY) const{
	int placed = 0, x, y;
	while(placed < count){
		x = getRandomInt(0, width - 1);
		y = getRandomInt(0, height - 1);

		if(map -> canPlaceTower(x, y)){
			map -> placeTower(x, y, ammo, lives);
			towerX.push_back(x);
			towerY.push_back(y);
			placed++;
		}
	}
}

bool MapGenerator::generateEntrances(int exitX, int exitY, int entranceCount, Cursor * cursor) const{
	int counter = 0, x, y;
	int tryCounter = 0;

	while(counter < entranceCount){
		if(exitX != 0 && exitX != width - 1){
			x = getRandomInt(0, width - 1);
			y = (exitY == 0)? height - 1 : 0; 
		}

		else if(exitY != 0 && exitY != height - 1){
			y = getRandomInt(0, height - 1);
			x = (exitX == 0)? width - 1 : 0;
		}

		else {
			x = getRandomInt(0, width - 1);
			y = getRandomInt(0, height - 1);
		}

		if(map -> makePath(x, y)){
			cursor -> setEntrance(x, y);
			counter ++;
		}

		tryCounter++;
		if(tryCounter > ALLOWED_TRIES * entranceCount)
			return false;
	}
	return true;
}