#include "map.h"

Map::Map() : width(0), height(0) {}

Map::~Map(){
	for(unsigned int i = 0; i < map.size(); i++)
		for(unsigned int j = 0; j < map[i].size(); j++)
			if(map[i][j] != nullptr) delete map[i][j];
}

void Map::setSize(int width, int height){
	this -> width = width;
	this -> height = height;

	map.resize(width);
	for(unsigned int i = 0; i < map.size(); i++)
		map[i].resize(height, nullptr);
}

void Map::getSize(int & width, int & height) const{
	width = this -> width;
	height = this -> height;
}

Entity * Map::getPosition(int x, int y) const{
	if(!isInRange(x, y)) return nullptr;
	return map[x][y];
}

void Map::setColor(int attackers, int towers, int walls){
	colorAttacker = attackers;
	colorTower = towers;
	colorWall = walls;
}

void Map::getColor(int & attackers, int & towers, int & walls) const{
	attackers = colorAttacker;
	towers = colorTower;
	walls = colorWall;
}

bool Map::canPlace(int x, int y) const{
	return isInRange(x,y) && map[x][y] == nullptr;
}

void Map::placeWall(int x, int y){
	bool horizontal;
	if(x == 0 || x == width - 1) horizontal = false;
	else if(y == 0 || y == height - 1) horizontal = true;

	else horizontal = getRandomBool();
	map[x][y] = new Wall(colorWall, horizontal);
}

bool Map::canPlaceGate(int x, int y) const{
	if(!canPlace(x, y)) return false;
	if(!canPlace(x + 1, y) && !canPlace(x - 1, y) 
		&& !canPlace(x, y + 1) && !canPlace(x, y - 1))
		return false;
	return true;
}

bool Map::canPlaceTower(int x, int y) const{
	if(!canPlace(x, y)) return false;
	if(shootRange(x, y, 0, 1) || shootRange(x, y, 0, -1)
		|| shootRange(x, y, 1, 0) || shootRange(x, y, -1, 0))
		return true;
	return false;
}

bool Map::canPlaceAttacker(int x, int y) const{
	if(isInRange(x, y) && map[x][y])
		return (map[x][y] -> behaviour() == PATH) || (map[x][y] -> behaviour() == EXIT);
	return false;
}

bool Map::shootRange(int x, int y, int directionX, int directionY) const{
	int i, j;
	for(i = x, j = y; isInRange(i, j); i += directionX, j += directionY){
		if(map[i][j] != nullptr){
			if(map[i][j] -> behaviour() == PATH ||
				map[i][j] -> behaviour() == ATTACK) return true;
			else return false;
		}
	}
	return false;
}


void Map::placeExit(int x, int y){
	map[x][y] = new Gate(GREEN, true);
}	

void Map::placeEntrance(int x, int y){
	map[x][y] = new Gate(WHITE, false);
}	

void Map::placeTower(int x, int y, int ammo, int lives){
	map[x][y] = new Tower(colorTower, ammo, lives, x, y);
}

Attacker * Map::placeAttacker(int x, int y, int ammo, int lives){
	Attacker * attacker = new Attacker(colorAttacker, ammo, lives, x, y);
	map[x][y] -> setVisitor(attacker);
	return attacker;
}

bool Map::makePath(int entranceX, int entranceY){
	if(!canPlaceGate(entranceX, entranceY)) return false;

	stack<Position*> path;
	if(!findPath(entranceX, entranceY, path)){
		while(!path.empty()){
			delete path.top();
			path.pop();
		}
		return false;
	}

	placeEntrance(entranceX, entranceY);

	Position * pos = nullptr;
	pos = path.top();
	path.pop();

	int x, y, lastX, lastY;
	if(pos) {
		pos -> getAncestor(lastX, lastY);
		delete pos;
	}

	while(!path.empty()){
		pos = path.top();
		path.pop();
		pos -> getPosition(x, y);

		if(x == lastX && y == lastY){
			if(map[x][y] == nullptr)
				map[x][y] = new Path(MAGENTA);
			pos -> getAncestor(lastX, lastY);
		}
		delete pos;
	}
	return true;
}

bool Map::findPath(int entranceX, int entranceY, stack<Position*> & path){
	vector<vector<int>> matrix;
	copyMatrix(matrix);

	queue<Position*> queue;
	queue.push(new Position(entranceX, entranceY));
	vector<int> move = { -1, 1 };

	int x, y;
	Position * p = nullptr;

	while(!queue.empty()){
		Position * pos = queue.front();
		queue.pop();		
		pos -> getPosition(x, y);

		matrix[x][y] = 1;
		path.push(pos);

		if(isExit(x, y)){
			while(!queue.empty()){
				delete queue.front();
				queue.pop();
			}
			return true;
		}

		for(unsigned int i = 0; i < move.size(); i++){
			if((x + move[i]) < width && (x + move[i]) >= 0 && matrix[x + move[i]][y] == 0){
				matrix[x + move[i]][y] = 1;
				p = new Position(x + move[i], y);
				p -> setAncestor(x, y);
				queue.push(p);
			}
			if((y + move[i]) < height && (y + move[i]) >= 0 && matrix[x][y + move[i]] == 0){
				matrix[x][y + move[i]] = 1;
				p = new Position(x, y + move[i]);
				p -> setAncestor(x, y);
				queue.push(p);
			}
		}
	}
	return false;
}

void Map::copyMatrix(vector<vector<int>> & matrix){
	matrix.resize(width);
	for(unsigned int i = 0; i < map.size(); i++){
		matrix[i].resize(height, 0);
		for(unsigned int j = 0; j < map[i].size(); j++)
			if(map[i][j] != nullptr && map[i][j] -> behaviour() != EXIT && map[i][j] -> behaviour() != PATH)
				matrix[i][j] = 2;
	}
}

bool Map::isExit(int x, int y)const{
	if(!isInRange(x, y) || !map[x][y]) return false;
	return map[x][y] -> behaviour() == EXIT;
}

bool Map::isInRange(int x, int y) const{
	if(x < 0 || y < 0 || x >= width || y >= height) 
		return false;
	return true;
}

void Map::move(int fromX, int fromY, int toX, int toY){
	Entity * entity = map[fromX][fromY] -> getVisitor();
	map[fromX][fromY] -> setVisitor(nullptr);
	
	if(map[toX][toY] -> behaviour() == EXIT)
		delete entity;
	else map[toX][toY] -> setVisitor(entity);
}

bool Map::placeBullet(int x, int y, bool shootTowers){
	if(!isInRange(x, y)) return false;
	if(map[x][y] == nullptr){
		map[x][y] = new Bullet();
		return true;
	}
	if(map[x][y] -> behaviour() == PATH){
		map[x][y] -> setVisitor(new Bullet());
		return true;
	}
	if(map[x][y] -> behaviour() == ENEMY){
		if(shootTowers == true){
			map[x][y] -> shot();
		}
		return false;
	}
	map[x][y] -> shot();
	return false;
}

bool Map::moveBullet(int x, int y, int toX, int toY, bool shootTowers){
	if(!isInRange(x, y) || !map[x][y]) return false;
	
	Entity * bullet = map[x][y] -> getVisitor();
	if(bullet == nullptr){
		bullet = map[x][y];
		map[x][y] = nullptr;
	}
	else map[x][y] -> setVisitor(nullptr);

	if(isInRange(toX, toY)){
		if(map[toX][toY] == nullptr){ 
			map[toX][toY] = bullet;
			return true;
		}
		else if(map[toX][toY] -> behaviour() == PATH){
			map[toX][toY] -> setVisitor(bullet);
			return true;
		}
		if(shootTowers || map[toX][toY] -> behaviour() != ENEMY)
			map[toX][toY] -> shot();
	} 
	if(bullet != nullptr) delete bullet;
	return false;
}	

void Map::removePosition(int x, int y){
	if(!isInRange(x, y)) return;
	if(!map[x][y]) return;
	if(map[x][y] -> getVisitor() != nullptr){
		delete map[x][y] -> getVisitor();
		map[x][y] -> setVisitor(nullptr);
		return;
	}
	delete map[x][y];
	map[x][y] = nullptr;
}

void Map::cleanMap(){
	for(unsigned int i = 0; i < map.size(); i++)
		for(unsigned int j = 0; j < map[i].size(); j++){
			if(map[i][j] != nullptr) delete map[i][j];
			map[i][j] = nullptr;
		}

}