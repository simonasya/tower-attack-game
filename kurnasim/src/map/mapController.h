#ifndef MAP_CONTROLLER
#define MAP_CONTROLLER

#include <string>
#include <iostream>
#include <map>
#include <cstdlib>
#include <ncurses.h>
#include <ctime>
#include <random>

#include "mapView.h"
#include "../file.h"
#include "../page/pageController.h"
#include "../score/scoreController.h"
#include "../entity/bulletController.h"
#include "cursor.h"
#include "../keys.h"
#include "../exceptionHandler.h"
#include "mapGenerator.h"

using namespace std;

const int TIMES_CLOCK = 100; /**<constant for clock mutliplication*/

/**
 * \brief controll map and its view and allows player interfere with the object 
 */

class MapController{	
public:
/**
 * \brief constructor of controller
 * \details initializes map, its view and score controller
 */
	MapController();
/**
 * \brief deletes alocated members
 */
	~MapController();
/**
 * \brief loads from level configuration file information of map
 * \param file file to read configuration from
 */
	void loadMap(string file);
/**
 * \brief show play map and takes input from player
 * \param title information about the level
 * \return -1 if escaped, 0 if lost, 1 if won
 */
	int show(string title);
/**
 * \brief calls mapView method for deletion of subwindow
 */
	void deleteMapBox();
/**
 * \brief creates map with static objects (walls, towers and gates)
 */
	void generateMap();
/**
 * \brief retrieve points of currently finished level
 * \return number of points
 */
	int Points()const;

private:
/**
 * \brief returns time 
 */
	int32_t getTime() const;
/**
 * \brief checks whether given time interval ended
 * \param count last time checked
 * \param freq time interval
 * \return true if ended, false otherwise
 */
	bool isTime(int32_t & count, int freq) const;
/**
 * \brief moves or shoots according to player command
 * \param command key chosen by player
 */
	bool performCommand(int command);
/**
 * \brief places bullet if possible and hands over to BulletController class
 * \param x X coordinate of shooter
 * \param y Y coordinate of shooter
 * \param directionX X direction of bullet
 * \param directionY Y direction of bullet
 */
	void shoot(int x, int y, int directionX, int directionY, bool shootTowers);
/**
 * \brief spesifies shoot position and direction of attacker 
 * \details calls shoot with the correct parametres
 */	
	void attackerShoot();
/**
 * \brief chooses direction of shooting for random of currently active towers
 * \details direction is chosen randomly for each of the towers
 * \param index of tower to shoot
 */
	void towerRandomShoot(int index);
/**
 * \brief picks index of tower at random and desides if shoots random or to shoots on path
 */
	void towerShoot();

	Map * map; /**<instance of current map*/
	MapView * mapView; /**<instance of mapview*/
	BulletController * bulletController; 
	ScoreController * score;
	Cursor cursor;
	
	std::map<string, int> configuration; /**<map for configuration level information*/
	vector<int> towerX; /**<tower X coordinate*/
	vector<int> towerY; /**<tower Y coordinate*/
};

#endif