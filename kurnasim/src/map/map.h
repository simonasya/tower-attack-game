#ifndef MAP
#define MAP

#include <vector>
#include <queue>
#include <stack>
#include <string>
#include <iostream>
using namespace std;

#include "../entity/wall.h"
#include "../entity/attacker.h"
#include "../entity/tower.h"
#include "../entity/gate.h"
#include "../entity/path.h"
#include "../entity/bullet.h"
#include "../functions.h"
#include "../file.h"
#include "position.h"

/**
 * \brief represents current map for objects in a level
 */


class Map{
public:
/**
 * \brief intialises empty instance of map
 */
	Map();
/**
 * \brief frees all used memory
 */
	~Map();
/**
 * \brief sets size of the map
 * \param width width of the map
 * \param height height of the map
 */
	void setSize(int width, int height);
/**
 * \brief returns size of the map
 * \param width width of the map
 * \param height height of the map
 */
	void getSize(int & width, int & height) const;
/**
 * \brief returns object on the position
 * \param x X coordinate
 * \param y Y coordinate
 * \return entity object if presents on the position, nullptr otherwise
 */
	Entity * getPosition(int x, int y)const;
/**
 * \brief sets colors of the objects
 * \param attackers color of attackers
 * \param towers color of towers
 * \param walls color of walls
 */
	void setColor(int attackers, int towers, int walls);
/**
 * \brief returns colors of the objects
 * \param attackers color of attackers
 * \param towers color of towers
 * \param walls color of walls
 */
	void getColor(int & attackers, int & towers, int & walls) const;
/**
 * \brief returns true if can place object on given position
 */
	bool canPlace(int x, int y) const;
/**
 * \brief returns true if can place gate on given position
 */
	bool canPlaceGate(int x, int y) const;
/**
 * \brief returns true if can place tower on given position
 */
	bool canPlaceTower(int x, int y) const;
/**
 * \brief returns true if can place attacker on given position
 */
	bool canPlaceAttacker(int x, int y) const;
/**
 * \brief finds out if tower can shoot directly to path without any obstacles
 * \param x X coordinate of tower
 * \param y Y coordinate of tower
 * \param directionX X direction of shooting
 * \param directionY Y direction of shooting
 * \return true if possible to reach path of attackers
 */
	bool shootRange(int x, int y, int directionX, int directionY) const;
/**
 * \brief creates new instance of wall on given position
 */
	void placeWall(int x, int y);
/**
 * \brief creates new instance of tower on given position
 */
	void placeTower(int x, int y, int ammo, int lives);
/**
 * \brief creates new instance of exit on given position
 */
	void placeExit(int x, int y);
/**
 * \brief creates new instance of entrance on given position
 */
	void placeEntrance(int x, int y);
/**
 * \brief creates new instance of attacker on given position
 * \return pointer to created attacker
 */
	Attacker * placeAttacker(int x, int y, int ammo, int lives);

/**
 * \brief creates path from given entrance to exit
 * \param entranceX X coordinate of entrance
 * \param entranceY Y coordinate of entrance
 * \return true if path exists
 */
	bool makePath(int entranceX, int entranceY);
/**
 * \brief runs BFS on map and looks for shortest path from entrance to exit
 * \param entranceX X coordinate of entrance
 * \param entranceY Y coordinate of entrance
 * \param path stores all visited positions - verticles
 * \return true if path was found
 */
	bool findPath(int entranceX, int entranceY, stack<Position*> & path);
/**
 * \brief returns true if given position is exit
 * \param x X coordinate
 * \param y Y coordinate
 * \return true if exit
 */	
	bool isExit(int x, int y)const;
/**
 * \brief checks whether given position is out of range
 * \param x X coordinate
 * \param y Y coordinate
 * \return if is in range of map
 */
	bool isInRange(int x, int y) const;
/**
 * \brief moves object from one position to another
 * \param fromX X source coordinate
 * \param fromY Y source coordinate
 * \param toX X destination coordinate
 * \param toY Y destination coordinate
 */
	void move(int fromX, int fromY, int toX, int toY);
/**
 * \brief places bullet to given position
 * \param x X coordinate
 * \param y Y coordinate
 * \param shootTowers true if capable of shooting towers
 * \return true if placement possible
 */
	bool placeBullet(int x, int y, bool shootTowers);
/**
 * \brief moves bullet from one position to another
 * \param fromX X source coordinate
 * \param fromY Y source coordinate
 * \param toX X destination coordinate
 * \param toY Y destination coordinate
 * \param shootTowers true if capable of shooting towers
 * \return if movement possible
 */
	bool moveBullet(int x, int y, int toX, int toY, bool shootTowers);
/**
 * \brief deletes object on given position
 */
	void removePosition(int x, int y);
/**
 * \brief cleans map from all the object
 * \details result is the map full of pointers
 */
	void cleanMap();
private:
/**
 * \brief copies map 2D vector for the needs of BFS algorithm
 * \param matrix 2D array
 */
	void copyMatrix(vector<vector<int>> & matrix);

	int colorAttacker; /**<color of attacker*/
	int colorTower; /**<color of tower*/
	int colorWall; /**<color of wall*/
	vector<vector<Entity*>> map; /**map of objects*/
	int width; /**width of map*/
	int height; /**height of map*/
};

#endif