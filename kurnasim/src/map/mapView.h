#ifndef MAP_VIEW
#define MAP_VIEW

#include <ncurses.h>
#include <iostream>
#include <unistd.h>

#include "map.h"
#include "../functions.h"
using namespace std;


const int TITLE_WINDOW_HEIGHT = 3; /**<height of title map window*/
const int TITLE_WINDOW_WIDTH = 10; /**<width of title map window*/

const int MAP_POSITION = 15; /**<Y coordinate of upper left map corner*/ 
const double INPUT_TIMEOUT = 0.01;  /**<timeout for one cycle of an input*/

/**
 * \brief represents user interface of the map
 */

class MapView{
public:
/**
 * \brief initialises map instance
 */
	MapView();
/**
 * \brief initialises map instance with the title
 * \param title title of map
 */
	MapView(string title);
/**
 * \brief frees allocated memory
 */
	~MapView();
/**
 * \brief sets title of map
 * \param title new title
 */
	void setTitle(string title);

/**
 * \brief shows interface for user
 * \param map resource for objects in map
 */
	void show(Map * map);
/**
 * \brief updates data in map
 * \param map resource for objects in map
 */
	void update(const Map * map) const;
/**
 * \brief deletes subwindow of map
 */
	void deleteMapBox();
/**
 * \brief shows map and waits for user input
 * \param map resource for objects in map
 * \return input chosen by user
 */
	int takeInput(Map * map);
private:
	string title; /**<map title*/
	WINDOW * mapWindow; /**<window for map*/
	WINDOW * titleWindow; /**<window for title*/ 
};

#endif