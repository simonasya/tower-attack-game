#include "mapView.h"

MapView::MapView() : mapWindow(nullptr), titleWindow(nullptr) {}

MapView::MapView(string title) 
: title(title), mapWindow(nullptr), titleWindow(nullptr) {}

MapView::~MapView(){
	deleteMapBox();
}

void MapView::setTitle(string title){
	this -> title = title;
}

void MapView::show(Map * map){
	if(!titleWindow) titleWindow = newwin(TITLE_WINDOW_HEIGHT, TITLE_WINDOW_WIDTH, 1, 1);
	mvwprintw(titleWindow, 1, 1, "%s", title.c_str());
	
	int width, height;
	if(!mapWindow){
		map -> getSize(width, height);
		mapWindow = newwin(height + 2, width + 2, 1, MAP_POSITION);
	}

	wattron(mapWindow, COLOR_PAIR(CYAN));
	box(mapWindow, '|', '-');
	wattroff(mapWindow, COLOR_PAIR(CYAN));

	update(map);
	wrefresh(titleWindow);
}

void MapView::update(const Map * map) const{
	int width, height;
	map -> getSize(width, height);

	for(int i = 0; i < width; i++){
		for(int j = 0; j < height; j++){
			Entity * tmp = map -> getPosition(i, j);
			if(tmp != nullptr){
				wattron(mapWindow, COLOR_PAIR(tmp -> getColor()));
				mvwprintw(mapWindow, j + 1, i + 1, "%c", tmp -> getRepresentation());
				wattroff(mapWindow, COLOR_PAIR(tmp -> getColor()));
			}
			else{
				mvwprintw(mapWindow, j + 1, i + 1, "%c", ' ');
			}
		}
	}
	wrefresh(mapWindow);
}

void MapView::deleteMapBox(){
	if(mapWindow)
		deleteBox(mapWindow);
	if(titleWindow)
		deleteBox(titleWindow);
}

int MapView::takeInput(Map * map){
	show(map);
	timeout(INPUT_TIMEOUT);
	return getch();
}