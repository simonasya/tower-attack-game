#ifndef MAP_GENERATOR
#define MAP_GENERATOR

#include "map.h"
#include "cursor.h"

const int ALLOWED_TRIES = 30;

/**
 * \brief generates static objects of given map
 */

class MapGenerator{
public:
/**
 * \brief initialises generator of map in a given size
 * \param width map width
 * \param height map height
 * \param map map to set objects of
 */
	MapGenerator(int width, int height, Map * map);
/**
 * \brief generates walls in a given number
 * \details picks places randomly and checks if placement is possible
 * \param count number of walls to be generated
 */
	void generateWalls(int count) const;
/**
 * \brief generates gates
 * \details calls generateEntrances()
 * \param entranceCount count of entrances to be generated
 * \param cursor cursor for setting up entrances for player
 * \return success of generating and finding a path between entrances and exit
 */
	bool generateGates(int entranceCount, Cursor * cursor) const;
/**
 * \brief generates towers of given number
 * \details for each tower picks place randomly and verifies if the placement is possible 
 * (if the tower is capable of shooting in path direction without any obstacles in a way)
 * \param count number of towers to be generated
 * \param ammo ammunition of the tower
 * \param lives lives of the tower
 * \param towerX saves X coordinates of towers
 * \param towerY saves Y coordinates of towers
 */
	void generateTowers(int count, int ammo, int lives, vector<int> & towerX, vector<int> & towerY) const;
private:
/**
 * \brief is called from generateGates()
 * \details generates given number of entrances and creates a path between entrances and exit
 * \param exitX already generated exit X coordinate
 * \param exitY already generated exit Y coordinate
 * \param entranceCount number of entrances to be generated
 * \param cursor object to remember entrance coordinates
 * \return success of finding a path in given tries specified by ALLOWED_TRIES constant
 */
	bool generateEntrances(int exitX, int exitY, int entranceCount, Cursor * cursor) const;
	int width; /**<width of the map*/
	int height; /**<height of the map*/
	Map * map; /**<given map to save objects*/
};

#endif