#include "mapController.h"

MapController::MapController() 
: map(new Map()), mapView(new MapView()), bulletController(new BulletController()), score(nullptr) {}

MapController::~MapController(){
	if(map) 
		delete map;
	if(mapView) 
		delete mapView;
	if(score) 
		delete score;
	if(bulletController) 
		delete bulletController;
}

void MapController::loadMap(string file){
	File * conf = new File(file);
	string valueName, value;

	if(!conf -> open())
		throw ExceptionHandler(file, FILE_OPEN_ERROR);

	while(!conf -> eof()){
		if(!conf -> parseFromFile(valueName, value)) break;
		configuration[valueName] = stoi(value);
	}
	delete conf;

	map -> setSize(configuration["width"], configuration["height"]);
	map -> setColor(configuration["colorAttacker"], configuration["colorTower"], configuration["colorWall"]);
	score = new ScoreController(configuration["attackers"], configuration["towers"]);
	generateMap();
}

int MapController::show(string title){
	if(!mapView) mapView = new MapView(title);
	else mapView -> setTitle(title);

	cursor.placeAttacker(score, map, configuration["attackerAmmo"], configuration["attackerLives"]);
	int32_t speedCount = getTime(), shootCount = getTime();

	while(!score -> win() && !score -> loss()){
		score -> show();
		int command = mapView -> takeInput(map);
		if(!performCommand(command)) 
			return -1;

		if(isTime(shootCount, configuration["shootFrequention"]))
			towerShoot();
		if(isTime(speedCount, configuration["shootSpeed"]))
			bulletController -> updatePosition(map, score);
		
		if(score -> loss()) return 0;
		if(score -> win()) return 1;

		if(cursor.attackerInExit(map)){
			score -> savedAttacker();
			if(score -> loss()) return 0;
			cursor.placeAttacker(score, map, configuration["attackerAmmo"], configuration["attackerLives"]);
		}
		
		if(cursor.attackerShot(map))
			cursor.placeAttacker(score, map, configuration["attackerAmmo"], configuration["attackerLives"]);
		
	}
	if(score -> win()) return 1;
	return 0;
}

int32_t MapController::getTime() const{
	return (int)(clock() * TIMES_CLOCK / (CLOCKS_PER_SEC));
}

bool MapController::isTime(int32_t & count, int freq) const{
	bool ret = getTime() - count >= freq;
	if(ret)
		count = getTime();
	return ret;
}

bool MapController::performCommand(int command){
	switch(command){
		case KEY_SHOOT_UP: {
			cursor.setShootDirection(0, -1); 
			attackerShoot(); break;
		}
		case KEY_SHOOT_LEFT: {
			cursor.setShootDirection(-1, 0);
			attackerShoot(); break;
		}
		case KEY_SHOOT_RIGHT: {
			cursor.setShootDirection(1, 0); 
			attackerShoot(); break;
		}
		case KEY_SHOOT_DOWN: {
			cursor.setShootDirection(0, 1); 
			attackerShoot(); break;
		}
		case KEY_RIGHT: cursor.moveRight(map);
		 				break;
		case KEY_LEFT: cursor.moveLeft(map);
						break;
		case KEY_UP: cursor.moveUp(map);
						break;
		case KEY_DOWN: cursor.moveDown(map);
						break;
		case KEY_ALT_ESC: return false;
	}
	return true;
}

void MapController::deleteMapBox(){
	if(mapView)
		mapView -> deleteMapBox();
}

void MapController::generateMap(){
	MapGenerator * generator = new MapGenerator(configuration["width"], configuration["height"], map);
	generator -> generateWalls(configuration["walls"]);

	while(!generator -> generateGates(configuration["entrances"], &cursor)){
		map -> cleanMap();
		cursor.cleanEntrances();
		generator -> generateWalls(configuration["walls"]);
	}
	generator -> generateTowers(configuration["towers"], configuration["towerAmmo"], configuration["towerLives"], towerX, towerY);
	delete generator;
}

void MapController::shoot(int x, int y, int directionX, int directionY, bool shootTowers){
	if(map -> getPosition(x, y) == nullptr || !map -> getPosition(x, y) -> shoot())
		return;

	x += directionX; 
	y += directionY;

	bool success = map -> placeBullet(x, y, shootTowers);
	mapView -> show(map);

	if(success)
		bulletController -> addBullet(x, y, directionX, directionY, shootTowers);
	else{
		Entity * obj = map -> getPosition(x, y);
		if(obj != nullptr && obj -> isDead()){
			if(obj -> behaviour() == ENEMY && shootTowers) (*score) ++;
			else if(obj -> behaviour() == ATTACK) (*score) --;
			map -> removePosition(x, y);
		}
	}
}

void MapController::attackerShoot(){
	int x, y, directionX, directionY;
	cursor.getCurrentPosition(x, y);
	cursor.getShootDirection(directionX, directionY);
	shoot(x, y, directionX, directionY, true);
}

void MapController::towerRandomShoot(int index){
	if(towerX.size() == 0) return;

	int directionX, directionY;
	if(getRandomBool()){
		directionX = 0;
		if(getRandomBool()) directionY = 1;
		else directionY = -1;
	}
	else {
		directionY = 0;
		if(getRandomBool()) directionX = 1;
		else directionX = -1;
	}
	shoot(towerX[index], towerY[index], directionX, directionY, false);
}

void MapController::towerShoot(){
	int index = getRandomInt(0, towerX.size() - 1);
	
	if(configuration["randomShoot"])
		return towerRandomShoot(index);

	if(map -> shootRange(towerX[index], towerY[index] + 1, 0, 1))
		return shoot(towerX[index], towerY[index], 0, 1, false);

	if(map -> shootRange(towerX[index] + 1, towerY[index], 1, 0))
		return shoot(towerX[index], towerY[index], 1, 0, false);
	
	if(map -> shootRange(towerX[index], towerY[index] - 1, 0, -1))
		return shoot(towerX[index], towerY[index], 0, -1, false);
	
	if(map -> shootRange(towerX[index] - 1, towerY[index], -1, 0))
		return shoot(towerX[index], towerY[index], -1, 0, false);
}

int MapController::Points()const{
	return score -> Points();
}