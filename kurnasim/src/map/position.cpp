#include "position.h"

Position::Position(int x, int y) : x(x), y(y) {}

void Position::getPosition(int & x, int & y) const{
	x = this -> x;
	y = this -> y;
}

void Position::getAncestor(int & ancestorX, int & ancestorY) const{
	ancestorX = this -> ancestorX;
	ancestorY = this -> ancestorY;
}

void Position::setAncestor(int ancestorX, int ancestorY){
	this -> ancestorX = ancestorX;
	this -> ancestorY = ancestorY;
}
