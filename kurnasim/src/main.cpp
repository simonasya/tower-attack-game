/**
 * \author Simona Kurnavova
 * \version 1.0 
 */
 
#ifndef MAIN
#define MAIN

#include <iostream>
#include <ncurses.h>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <exception>
using namespace std;

#include "game.h"
#include "file.h"

/**
 * \brief Initialization of new game
 */
 
int main(void){
	try{
		Game * game = new Game();
		game -> loadConfig();
		game -> showMenu();
		delete game;
	}
	catch(exception & e){
		File * logFile = new File(LOG_FILE);
		logFile -> open();
		logFile -> write(e.what());
		delete logFile;
		endwin();
		return -1;
	}
	return 0;
}

#endif