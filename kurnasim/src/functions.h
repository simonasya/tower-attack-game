#ifndef FUNCTIONS
#define FUNCTIONS

#include <cstdlib>
#include <string>
#include <random>
#include <ncurses.h>
#include "colors.h"

using namespace std;

/**
 * \brief clears and destroyes windows of ncurses
 * \details clears borders and symbols and calls endwin()
 * \param W window to destroy
 */
void deleteBox(WINDOW *);

/**
 * \brief prints text in a chosen color to window
 * \param W window to print to
 * \param g string to print
 * \param t X coordinate
 * \param t Y coordinate
 * \param t color of text
 */
void writeToWindow(WINDOW *, string, int, int, int);

/**
 * \brief genrerates random bool value
 * \return random bool
 */
bool getRandomBool();

/**
 * \brief generates random int value
 * \param t minimum of number
 * \param t maximum of number
 * 
 * \return random int
 */
int getRandomInt(int, int);

#endif