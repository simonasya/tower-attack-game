#include "form.h"

Form::Form(const string & title, const string & request)
: title(title), request(request), formView(nullptr) {}

Form::~Form(){
	delete formView;
}

string Form::getTitle(){
	return title;
}

string Form::getRequest(){
	return request;
}

void Form::show(){
	if(!formView) 
		formView = new FormView();
	formView -> show(title, request);

}

void Form::deleteFormBox(){
	if(formView)
		formView -> deleteFormBox();
}

string Form::getParsed() const{
	return formView -> getParsed();
}