#include "functions.h"

using namespace std;

void deleteBox(WINDOW * w){
	box(w, ' ', ' ');
	werase(w);
	wrefresh(w);
	delwin(w);
}

void writeToWindow(WINDOW * w, string text, int x, int y, int color){
	wattron(w, COLOR_PAIR(color));
		mvwprintw(w, y, x, "%s", text.c_str());
	wattroff(w, COLOR_PAIR(color));
}

bool getRandomBool(){
	random_device rd;
	mt19937 rng(rd());
	uniform_int_distribution<int> uni;

	if(uni(rng) % 2 == 0) return true;
	return false;
}

int getRandomInt(int min, int max){
	random_device rd;
	mt19937 rng(rd());
	uniform_int_distribution<int> uni(min, max);
	return uni(rng);
}