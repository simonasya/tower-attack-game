#include "windowController.h"

WindowController::WindowController(){
	window = initscr();
	noecho();
	raw();
	curs_set(false);
	keypad(window, true);
	start_color();

	init_pair(RED, RED, BLACK);
	init_pair(GREEN, GREEN, BLACK);
	init_pair(YELLOW, YELLOW, BLACK);
	init_pair(BLUE, BLUE, BLACK);
	init_pair(MAGENTA, MAGENTA, BLACK);
	init_pair(CYAN, CYAN, BLACK);
	init_pair(WHITE, WHITE, BLACK);
	
	wrefresh(window);
}

WindowController::~WindowController(){
	refresh();
	delwin(window);
	endwin();
}

WINDOW * WindowController::getWindow(){
	return window;
}