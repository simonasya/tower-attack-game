#ifndef FORM_VIEW
#define FORM_VIEW

#include <ncurses.h>
#include <cstring>
#include "functions.h"

const int FORM_WIDTH = 30;
const int FORM_HEIGHT = 5;

/**
 * \brief handles user interface of Form class
 */

class FormView{
public:
/**
 * \brief creates view instance
 */
	FormView();
/**
 * \brief frees memory and window
 */
	~FormView();
/**
 * \brief shows the form and waits for user input
 * \param title title of form
 * \param request request for user
 */
	void show(string title, string request);
/**
 * \brief deletes window used by form
 */
	void deleteFormBox();
/**
 * \brief possible to call only after show()
 * \return user input to form
 */
	string getParsed() const;
private:
	WINDOW * formWindow; /**<window of form*/
	string parsed; /**<user input*/
};

#endif