#ifndef REPRESENTATION
#define REPRESENTATION

const char ATTACKER_REPRESENTATION = 'o';
const char TOWER_REPRESENTATION = 'T';
const char WALL_REPRESENTATION = '#';
const char GATE_REPRESENTATION = 'x';
const char PATH_REPRESENTATION = '.';
const char BULLET_REPRESENTATION = '*';

#endif