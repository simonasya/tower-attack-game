#ifndef KEYS
#define KEYS

const int KEY_SHOOT_UP = 119;
const int KEY_SHOOT_DOWN = 115;
const int KEY_SHOOT_LEFT = 97;
const int KEY_SHOOT_RIGHT = 100;

const int KEY_ENTER1 = 0xa; /**<mapping of enter for ncurses*/
const int KEY_ENTER2 = 0xa03; /**<mapping of numeric enter for ncurses*/
const int KEY_ALT_ESC = 27; /**<mapping of alt and esc for ncurses*/


#endif