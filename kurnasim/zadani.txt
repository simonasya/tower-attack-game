Naprogramujte jendoduchou grafickou tower ATTACK hru

Váš engine:

    ze souboru nahraje definici ÚTOČNÍKŮ (jméno, životy rychlost, útok, dosah, ...)
    ze souboru nahraje možné mapy a typy obranných věží (životy, rychlost, odolnnost na určitý typ věže ,...)
    implementuje jednoduchou interakci věž vs. útočníci (útok, proběhnutí, ...), počitadlo skore, detekci vítěztví (po nezabití x útočníků)
    implementuje jednoduchou AI řídící rozisťování věží
    umožňuje ukládat a načítat rozehrané hry

Engine může fungovat real-time hra, či tahová hra.

Jak může vypadat mapa?

" "prázdné místa pro pohyb útočníku a stavbu věží, I označuje věže, # označuje zeď, @ a % jsou různé druhy útočníků, <= jsou vstupy/výstupy z mapy.

 
#################################################
#                        #       @@  #   @@  @@<=3
#         #              #    %I  @  ###        #
#         #              #    %#  @    #    I   #
<=%%%     #              I    %#       #@  @@  <=1
#         #              I    %#       @@   I   #
#                        I    %#                #
#                 %%%       %%%#    @          <=2
#################################################                      

Cesta kudy se budu útočníci ubírat bude vždy nejkratší možná vzhledem ke zdím a věžím

Hráč volí vstup a typy útočníků

Kde lze využít polymorfismus? (doporučené)

    Parametry útočníků: znak, barva, rychlst, životy, ...
    Efekty útoku věží: zranění, zpomalení, ...
    Políčka mapy: volno, věž, útočník ...
    Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), ...

Ukázky:

    Obecné informace, požadavky, ...
    http://en.wikipedia.org/wiki/Tower_defense
    http://store.steampowered.com/app/251530/
    http://www.youtube.com/watch?v=4eV4918kReE
    http://www.newgrounds.com/portal/view/576124
    Mail: D. Bernhauer
